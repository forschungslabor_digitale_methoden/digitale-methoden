import os,sys
import os.path as op
import datetime
import zipfile
import io
import pathlib
from flask import Flask, flash, send_file, url_for
from flask_sqlalchemy import SQLAlchemy
from jinja2 import Template
from markupsafe import Markup


from sqlalchemy import cast, Integer, Text, Column, ForeignKey, literal, null
from sqlalchemy.orm import sessionmaker, relationship, aliased
from sqlalchemy.sql import column, label, expression, functions
from sqlalchemy.ext.hybrid import hybrid_property

from werkzeug.utils import redirect
from wtforms import validators

import flask_admin as admin
from flask_admin.base import MenuLink, AdminIndexView
from flask_admin.contrib import sqla
from flask_admin.contrib.sqla import filters
from flask_admin.contrib.sqla.form import InlineModelConverter
from flask_admin.contrib.sqla.fields import InlineModelFormList
from flask_admin.contrib.sqla.filters import BaseSQLAFilter, FilterEqual
from flask_admin.actions import action

# Create application
app = Flask(__name__)

# set optional bootswatch theme
# see http://bootswatch.com/3/ for available swatches
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'

# Create dummy secrey key so we can use sessions
app.config['SECRET_KEY'] = '123456790'

# Create in-memory database
app.config['DATABASE_FILE'] = 'sample_db.sqlite'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + app.config['DATABASE_FILE']
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# Create M2M table
software_features_table = db.Table('software_features', db.Model.metadata,
                                   db.Column('software_id', db.Integer, db.ForeignKey('software.id')),
                                   db.Column('feature_id', db.Integer, db.ForeignKey('feature.id')))
# Create N2one table
software_languages_table = db.Table('software_languages', db.Model.metadata,
                                  db.Column('software_id', db.Integer, db.ForeignKey('software.id')),
                                  db.Column('language_id', db.Integer, db.ForeignKey('language.id')))
# Create N2one table
software_programminglanguages_table = db.Table('software_programminglanguages', db.Model.metadata,
                                  db.Column('software_id', db.Integer, db.ForeignKey('software.id')),
                                  db.Column('programminglanguage_id', db.Integer, db.ForeignKey('programminglanguage.id')))
# Create N2one table
software_operatingsystems_table = db.Table('software_operatingsystems', db.Model.metadata,
                                  db.Column('software_id', db.Integer, db.ForeignKey('software.id')),
                                  db.Column('operatingsystem_id', db.Integer, db.ForeignKey('operatingsystem.id')))
# Create M2M table
software_methods_table = db.Table('software_methods', db.Model.metadata,
                                   db.Column('software_id', db.Integer, db.ForeignKey('software.id')),
                                   db.Column('method_id', db.Integer, db.ForeignKey('method.id')))

class License(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    version = db.Column(db.String(100))

    def __str__(self):
        return "{}".format(self.name)if not self.version else "{}-{}".format(self.name, self.version)

    def __repr__(self):
        return "{}: {}".format(self.id, self.__str__())


class Feature(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64))

    def __str__(self):
        return "{}".format(self.name)


class Language(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Enum('german', 'english', 'french', 'spanish', 'chinese', 'russian', 'other', name='languages'))

    def __str__(self):
        return "{}".format(self.name)

class Operatingsystem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Enum('windows', 'linux', 'osx', 'ios', 'android', 'browser', 'docker', name='languages'), unique=True)

    def __str__(self):
        return "{}".format(self.name)


class Programminglanguage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64))

    version = db.Column(db.String(100))

    def __str__(self):
        return "{}".format(self.name)


class Method(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    description =  db.Column(db.String(1024))
    parent_id = db.Column(db.Integer, db.ForeignKey('method.id'))
    parent = db.relationship('Method', remote_side=[id], backref='children')

    def __str__(self):
        return "{}".format(self.name)

class Othercollections(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    url = db.Column(db.Text)

    def __str__(self):
        return "{}".format(self.name)


class SoftwareCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    short_description = db.Column(db.Text)

    def __str__(self):
        return "{}".format(self.name)


class Software(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    short_description = db.Column(db.String(120))
    developer = db.Column(db.String(120))
    maintainer = db.Column(db.String(120))
    lastchanged = db.Column(db.Date)
    currentversion = db.Column(db.String(64))

    price = db.Column(db.Integer())
    modelprice = type = db.Column(db.Enum('yearly', 'once', name='modelprice'))

    license_id = db.Column(db.Integer(), db.ForeignKey(License.id))
    license = db.relationship(License, backref='softwares')

    softwarecategory_id = db.Column(db.Integer(), db.ForeignKey(SoftwareCategory.id))
    softwarecategory = db.relationship(SoftwareCategory, backref='software')

    features = db.relationship('Feature', secondary=software_features_table)

    languages = db.relationship('Language', secondary=software_languages_table)

    programminglanguages = db.relationship('Programminglanguage', secondary=software_programminglanguages_table)

    architecture = db.Column(db.Enum('stand-alone application',
                                     'server application',
                                     'framework',
                                     'library',
                                     'app',
                                     'plugin',
                                     'other', name='software_types'))

    operatingsystems = db.relationship('Operatingsystem', secondary=software_operatingsystems_table)

    methods  = db.relationship('Method', secondary=software_methods_table)

    recommandedcitation = db.Column(db.String(120))

    def __str__(self):
        return "{}".format(self.name)


class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Enum('repository', 'website', 'developer', 'tutorial', 'wiki', 'faq', 'other', name='link_types'))

    url = db.Column(db.String(120))

    software_id = db.Column(db.Integer(), db.ForeignKey(Software.id))
    software = db.relationship(Software, backref='links')

    comment = db.Column(db.String(120))

    def __str__(self):
        return "<a href='#'>{}</a>:{}".format(self.type,self.url)


class Reference(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64))
    cited = db.Column(db.Unicode(64))

    def __str__(self):
        return "{}".format(self.name)


# Flask views
#@app.route('/')
#def index():
#    return redirect(url_for("admin.index"))
@app.route('/wiki-export')
def index():
    # Generate sub pages for tools
    with open('templates/export/software.jinja2', "r", encoding="utf-8") as file_:
        template = Template(file_.read())
    softwares = Software.query.all()
    for software_tool in softwares:
        template.stream(software=software_tool).dump(
            '../digitale_Methoden.wiki/Tool_' + software_tool.name.replace(' ', '').replace('/', '') + '.asciidoc', encoding='utf-8')

    # select software tools for overview page
    standalonesoftwareincategory = []
    serversoftwareincategory = []
    libsoftwareincategory = []
    software_categories = SoftwareCategory.query.all()

    # Generate categories overview page
    with open('templates/export/softwares.jinja2', "r", encoding="utf-8") as file_:
        template = Template(file_.read())
        template.stream(software_categories=software_categories).dump(
        '../digitale_Methoden.wiki/SoftwareToolsList.asciidoc', encoding='utf-8')

    for software_category in software_categories:
        # standalone software
        standalonesoftwareincategory = Software.query.filter((Software.softwarecategory_id == software_category.id) & (Software.architecture == "stand-alone application"))
        # server software
        serversoftwareincategory = Software.query.filter((Software.softwarecategory_id == software_category.id) & (Software.architecture == "server application"))
        # other software

        softwares_r = Software.query.filter((Software.softwarecategory_id == software_category.id) & ((Software.architecture != "stand-alone application") & (Software.architecture != "server application")) & Software.programminglanguages.any(Programminglanguage.name=="R"))
        softwares_py = Software.query.filter((Software.softwarecategory_id == software_category.id) & ((Software.architecture != "stand-alone application") & (Software.architecture != "server application")) & Software.programminglanguages.any(name="Python"))
        softwares_misc = Software.query.filter((Software.softwarecategory_id == software_category.id) & ((Software.architecture != "stand-alone application") & (Software.architecture != "server application")) & (~ (Software.programminglanguages.any(name="Python") | Software.programminglanguages.any(name="R"))))
        libsoftwareincategory = (softwares_r, softwares_py, softwares_misc)
        # Generate tools in category overview page
        with open('templates/export/softwares_categories.jinja2', "r", encoding="utf-8") as file_:
            template = Template(file_.read())
            template.stream(software_category_name= software_category.name,
                            software_category_short_description=software_category.short_description,
                            standalonesoftwareincategory= standalonesoftwareincategory,
                            serversoftwareincategory= serversoftwareincategory,
                            libsoftwareincategory= libsoftwareincategory).dump(
            '../digitale_Methoden.wiki/SoftwareCategory_' + software_category.name.replace(' ', '').replace('/', '') + '.asciidoc', encoding='utf-8')

    # Generate methods overview page
    hierarchy = db.session.query(Method, literal(0).label('level')).filter(Method.parent_id == null()) \
        .cte(name="hierarchy", recursive=True)

    parent = aliased(hierarchy, name="p")
    children = aliased(Method, name="c")
    hierarchy = hierarchy.union_all(
        db.session.query(
            children,
            (parent.c.level + 1).label("level"))
            .filter(children.parent_id == parent.c.id))

    result = db.session.query(hierarchy.c.level, hierarchy.c.id, hierarchy.c.parent_id, hierarchy.c.name,
                              hierarchy.c.description) \
        .select_entity_from(hierarchy).order_by(hierarchy.c.id) \
        .all()
    references = db.session.query(Reference).order_by(Reference.name).all()

    # Generate sub pages
    with open('templates/export/MethodsList.jinja2', "r", encoding="utf-8") as file_:
        template = Template(file_.read())
    template.stream(methods=result, references=references).dump('../digitale_Methoden.wiki/MethodsList.asciidoc',
                                                                encoding='utf-8')

    base_path = pathlib.Path('../digitale-Methoden-wiki/')
    #data = io.BytesIO()
    #with zipfile.ZipFile(data, mode='w') as z:
    #    for f_name in base_path.iterdir():
    #        z.write(f_name)
    #data.seek(0)

    #return send_file(
    #    data,
    #    mimetype='application/zip',
    #    as_attachment=True,
    #    attachment_filename='data.zip'
    #)
    flash("Wiki-pages exported to " + str(base_path))
    return redirect(url_for("admin.index"))


class AdvancedSoftwareView(sqla.ModelView):
    column_sortable_list = ('name', ('license', ("license.name", "license.version")) , ('softwarecategory', 'softwarecategory.name'), 'lastchanged', 'architecture', )
    column_list = ('name', 'license', 'softwarecategory', 'links', 'architecture', 'programminglanguages', 'price', )
    inline_models = (Link,)
    column_hide_backrefs = False
    page_size = 100

    def _links_formatter(view, context, model, name):
        form_links = []
        for link in model.links:
            form_link = "<a href='{}' target='_blank'>{}</a>".format(link.url,link.type)
            if link.comment:
                form_link = "<a href='{}' target='_blank'>{}-{}</a>".format(link.url,link.type,link.comment)
            form_links.append(form_link)
        return Markup(','.join(form_links))

    column_formatters = {
        'links': _links_formatter
    }

# Create admin
admin = admin.Admin(app, name='digital methods:software-tools', template_mode='bootstrap3', index_view=AdminIndexView(
        name='home',
        template='admin/index.html',
        url='/'
    ))

# Add views
admin.add_view(AdvancedSoftwareView(Software, db.session, name="software-tools"))
admin.add_view(sqla.ModelView(Method, db.session, name="methods"))
admin.add_view(sqla.ModelView(Othercollections, db.session, name="other collections"))
admin.add_view(sqla.ModelView(Feature, db.session, name="software-features", category="miscellaneous"))
admin.add_view(sqla.ModelView(License, db.session, name="licenses", category="miscellaneous"))
admin.add_view(sqla.ModelView(Link, db.session, name="links", category="miscellaneous"))
admin.add_view(sqla.ModelView(SoftwareCategory, db.session, name="software categories", category="miscellaneous"))
admin.add_link(MenuLink(name="wiki-export", url='/wiki-export', category="miscellaneous"))



def build_sample_db():
    """
    Populate a small db with some example entries.
    """

    db.drop_all()
    db.create_all()

    collection = Othercollections(name="CRAN-R",
                                  url="https://cran.r-project.org/web/views/")
    db.session.add(collection)
    collection = Othercollections(name="ROpenSci",
                                  url="https://ropensci.org/packages/")
    db.session.add(collection)
    collection = Othercollections(name="Text Visualization Browser",
                                  url="http://textvis.lnu.se/")
    db.session.add(collection)

    method = Method(id=1,
                    name="digital methods",
                    description="""<<Rogers2013>> distinguishes between digitalized/virtual and digital methods. The former methods import standard methods from the social sciences and humanities into the emerging medium. The latter are completly new methods which emerge following the new structures and their properties. +
In this project a more inclusive conception of digital methods is assumed: the use of digital technology or technique during the research.""",
                    parent=None)
    db.session.add(method)
    method1 = Method(id=2,
                     name="data mining",
                     description="""Refers to the complete process of 'knowledge mining from data'.<<Han_etal2012>> Can be applied on various data types and consists of different steps and paradigms. For an application in the context of text mining in the social science see the concept "blended-reading" (<<Stulpe_etal2016>>).""",
                     parent=method)
    db.session.add(method1)
    method2 = Method(id=3,
                     name="data wrangling",
                     description="Translate data into suited formats for automatic analysis. Examples: PDFs ⇒ Text . For a practical framework refer also <<Wickham_etal2017>>.",
                     parent=method1)
    db.session.add(method2)
    method3 = Method(id=4,
                     name="regular expressions",
                     description="Complex string manipulations by searching and replacing specific patterns.",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=5,
                     name="data-format conversions",
                     description="Transfer between different formats in order to unify and handle vacancies.",
                     parent=method2)
    db.session.add(method3)
    m_algotextex = Method(id=6,
                     name="algoritmic text extraction",
                     description="Automated text extraction from markup language. See: (<<Günther_etal2014>>,117)",
                     parent=method2)
    db.session.add(m_algotextex)
    method3 = Method(id=7,
                     name="body text extraction",
                     description="Wrangling on structure based features of webpages. See: (<<Günther_etal2014>>,118)",
                     parent=m_algotextex)
    db.session.add(method3)
    method3 = Method(id=8,
                     name="boilerpipe",
                     description="More local approuch than body text extraction (<<Günther_etal2014>>,118)",
                     parent=m_algotextex)
    db.session.add(method3)
    method3 = Method(id=9,
                     name="jusText",
                     description="A heuristic based boilerplate removal tool See: (<<Pomikálek2011>>)",
                     parent=m_algotextex)
    db.session.add(method3)
    m_text_preprocessing = Method(id=10,
                     name="text preprocessing",
                     description="Some text preprocessing tasks in natuaral language processing.",
                     parent=method1)
    db.session.add(m_text_preprocessing)
    m_tokenization = Method(id=11,
                     name="tokenization",
                     description="Identify words in character input sequence.",
                     parent=m_text_preprocessing)
    db.session.add(m_tokenization)
    m_stopword_removal = Method(id=12,
                     name="stop-word removal",
                     description="Removing high-frequency words like pronoums, determiners or prepositions.",
                     parent=m_text_preprocessing)
    db.session.add(m_stopword_removal)
    m_stemming = Method(id=13,
                     name="stemming",
                     description="Identify common stems on a syntactical level.",
                     parent=m_text_preprocessing)
    db.session.add(m_stemming)
    m_word_sentence_segmentation = Method(id=14,
                     name="word/sentence segmentation",
                     description="Separate a chunk of continuous text into separate words/sentences.",
                     parent=m_text_preprocessing)
    db.session.add(m_word_sentence_segmentation)
    m_pos_tagging = Method(id=15,
                     name="part-of-speech(POS)-tagging",
                     description="Identify the part of speech for words.",
                     parent=m_text_preprocessing)
    db.session.add(m_pos_tagging)
    m_dependency_parsing = Method(id=16,
                     name="dependency parsing",
                     description="Create corresponding syntactic, semantic or morphologic trees from input text.",
                     parent=m_text_preprocessing)
    db.session.add(m_dependency_parsing)
    m_syntactic_parsing = Method(id=17,
                     name="syntactic parsing",
                     description="Create syntactic trees from input text using mostly unsupervised learning on manually annotated treebanks (<<Ignatow_etal2017>>,61).",
                     parent=m_dependency_parsing)
    db.session.add(m_syntactic_parsing)
    m_word_sense_disambiguation = Method(id=18,
                     name="word-sense disambiguation",
                     description="Recognizing context-sensetive meaning of words.",
                     parent=m_text_preprocessing)
    db.session.add(m_word_sense_disambiguation)
    method2 = Method(id=19, name="information extraction",
                     description="Extract factual information(e.g. people, places or situations) in free text.",
                     parent=method1)
    db.session.add(method3)
    method3 = Method(id=20,
                     name="(named-)entity-recognition/resolution/extraction/tagging",
                     description="Identify instances of specific (pre-)defined types(e.g place, name or color) in text.",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=21,
                     name="relation extraction",
                     description="Extract relationships between entities.",
                     parent=method3)
    db.session.add(method3)
    method2 = Method(id=22,name="information retrieval",
                     description="Retrieve relevant informations in response to the information requests.",
                     parent=method1)
    db.session.add(method2)
    m_indexing = Method(id=23,name="indexing",
                     description="'organize data in such a way that it can be easily retrieved later on'(<<Ignatow_etal2017>>,137)",
                     parent=method2)
    db.session.add(m_indexing)
    m_search_query = Method(id=24,name="searching/querying",
                     description="'take information requests in the form of queries and return relevant documents'(<<Ignatow_etal2017>>,137). There are different models in order to estimate the similarity between records and the search queries (e.g. boolean, vector space or a probabilistic model)(ibid).",
                     parent=method2)
    db.session.add(m_search_query)
    method2 = Method(id=25,name="statistical analysis",
                     description="",
                     parent=method1)
    db.session.add(method2)
    m_frequency_analysis = Method(id=26,name="frequency analysis",
                     description="Descriptiv statistical analysis by using specific text abundances.",
                     parent=method2)
    db.session.add(m_frequency_analysis)
    m_w_dict_frequencies = Method(id=27,name="word frequencies/dictionary analysis",
                     description="Analyse statistical significant occurence of words/word-groups. Can also be combined with meta-data (e.g. creation time of document).",
                     parent=m_frequency_analysis)
    db.session.add(m_w_dict_frequencies)
    m_co_occurence = Method(id=28,name="co-occurence analysis",
                     description="Analyse statistical significant co-occurence of words in different contextual units.",
                     parent=m_frequency_analysis)
    db.session.add(m_co_occurence)
    m_context_volatility = Method(id=29,name="context volatility",
                     description="'Analyse contextual change for certain words over a period of time.'(<<Niekler_etal2018>>,1316)",
                     parent=m_frequency_analysis)
    db.session.add(m_context_volatility)
    method3 = Method(id=30,name="classification/statistical learning",
                     description="Various techniques to (semi-)automatically identify specific classes. ",
                     parent=method2)
    db.session.add(method3)
    m_supervised_classification = Method(id=31,name="supervised classification",
                     description="Use given training examples in order to classify certain entities.",
                     parent=method3)
    db.session.add(m_supervised_classification)
    method4 = Method(id=32,name="latent semantic analysis",
                     description="'The basic idea of latent semantic analysis (LSA) is, that text do have a higher order (=latent semantic) structure which, however, is obscured by word usage (e.g. through the use of synonyms or polysemy). By using conceptual indices that are derived statistically via a truncated singular value decomposition (a two-mode factor analysis) over a given document-term matrix, this variability problem can be overcome.'(link:https://cran.r-project.org/web/packages/lsa/lsa.pdf[CRAN-R])",
                     parent=method3)
    db.session.add(method4)
    method4 = Method(id=33,name="topic modeling",
                     description="Probabilistic models to infer semantic clusters. See especially <<Papilloud_etal2018>>.",
                     parent=method3)
    db.session.add(method4)
    lda = Method(id=34,name="latent dirichlet allocation",
                     description="""'The application of LDA is based on three nested concepts: the text collection to be modelled is referred to as the corpus; one item within the corpus is a document, with words within a document called terms.(...) +
The aim of the LDA algorithm is to model a comprehensive representation of the corpus by inferring latent content variables, called topics. Regarding the level of analysis, topics are heuristically located on an intermediate level between the corpus and the documents and can be imagined as content-related categories, or clusters. (...) Since topics are hidden in the first place, no information about them is directly observable in the data. The LDA algorithm solves this problem by inferring topics from recurring patterns of word occurrence in documents.'(<<Maier_etal2018>>,94)""",
                     parent=method4)
    db.session.add(lda)
    nmf = Method(id=35,name="non-negative-matrix-factorization",
                     description="Inclusion of non-negative constraint.",
                     parent=method4)
    db.session.add(nmf)
    stm = Method(id=36,name="structural topic modeling",
                     description="Inclusion of meta-data. Refer especially to <<roberts2013>>.",
                     parent=method4)
    db.session.add(stm)
    sa = Method(id=37,name="sentiment analysis",
                     description="'Subjectivity and sentiment analysis focuses on the automatic identification of private states, such as opinions, emotions, sentiments, evaluations, beliefs, and speculations in natural language. While subjectivity classification labels text as either subjective or objective, sentiment classification adds an additional level of granularity, by further classifying subjective text as either positive, negative, or neutral.' (<<Ignatow_etal2017>> pp. 148)",
                     parent=method3)
    db.session.add(sa)
    method4 = Method(id=38,name="automated narrative, argumentative structures, irony, metaphor detection/extraction",
                     description="For automated narrative methapor analysis see (<<Ignatow_etal2017>>, 89-106. For argumentative structures(Task: Retrieving sentential arguments for any given controversial topic) <<Stab_etal2018>> .Refer for a current overview <<Cabrio2018>>.",
                     parent=method3)
    db.session.add(method4)
    method3 = Method(id=39,name="network analysis/modeling",
                     description="Generate networks out of text/relationships between text.",
                     parent=method2)
    db.session.add(method3)
    method4 = Method(id=40, name="knowledge graph construction",
                     description="Modelling entities and their relationships.",
                     parent=method3)
    db.session.add(method4)
    method2 = Method(id=41,name="data visualization",
                     description="Visualize the mined informations.",
                     parent=method1)
    db.session.add(method2)
    method3 = Method(id=42,name="word relationships",
                     description="",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=43,name="networks",
                     description="",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=44,name="geo-referenced",
                     description="",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=45,name="dynamic visualizations",
                     description="Visualizations with user interaction or animations.",
                     parent=method2)
    db.session.add(method3)
    method1 = Method(id=46,name="digital research practice in social or economic sciences",
                     description="",
                     parent=method)
    db.session.add(method1)
    m_automated_data_collection = Method(id=47,
                     name="automated data collection",
                     description="""In principal there are multiple possible data sources in a data mining process. A basic distinction in relevance to automated data collection can be drawn between connected devices(internet, intranets) or unconnected devices(sensors, etc.). +
    Furthermore the server-client-model is the established communication paradigms for connected devices. In order to obtain data either from server or client there exists three different interfaces: log files, apis and user interfaces which constitute the available procedures <<Jünger2018>>.""",
                     parent=method1)
    db.session.add(m_automated_data_collection)
    m_collect_log = Method(id=48,
                     name="collect log-data",
                     description="Collect log data which occur during providing the (web-)service or the information processing.",
                     parent=m_automated_data_collection)
    db.session.add(m_collect_log)
    m_parsing = Method(id=49,
                     name="parsing from api",
                     description="Parse structured data from via a documented REST-API.",
                     parent=m_automated_data_collection)
    db.session.add(m_parsing)
    m_scraping = Method(id=50,
                     name="scraping",
                     description="Automatically parse unstructured or semi-structured data from a normal website (⇒ web-scraping) or service.",
                     parent=m_automated_data_collection)
    db.session.add(m_scraping)
    m_scraping_stat = Method(id=51,
                     name="scraping (static content)",
                     description="Automatically parse data from static HTML websites.",
                     parent=m_scraping)
    db.session.add(m_scraping_stat)
    m_scraping_dyn = Method(id=52,
                     name="scraping (dynamic content)",
                     description="Automatically parse dynamic content (HTML5/Javascript,) ⇒ sometimes requires mimicking user-interaction.",
                     parent=m_scraping)
    db.session.add(m_scraping_dyn)
    m_crawling = Method(id=53,
                     name="crawling",
                     description="Collect websites with an initial set of webpages by following contained links <<Ignatow_etal2017>>.",
                     parent=m_automated_data_collection)
    db.session.add(m_crawling)
    m_tracking = Method(id=54,
                     name="tracking",
                     description="User-informed passive data collection.",
                     parent=m_automated_data_collection)
    db.session.add(m_tracking)
    m_esm_ema = Method(id=55,name="ecological momentary assessments (EMA)/Experience Sampling Method (ESM)",
                     description="EMA and ESM are mostly equivalent. EMA focusses on medical questions or measurements in a natural environment; ESM more on subjective Questions in the real life. Four characteristics: 1) data collection in natural environments 2) Focussing on near events/impressions/actions 3) questions triggered randomly or event-based 4) multiple questions over a certain period of time [Citation after Stone and Shiffmann 1994] (<<Salganik2018>>,109)",
                     parent=m_tracking)
    db.session.add(m_esm_ema)
    method2 = Method(id=56,name="blended reading/text mining",
                     description="Application of data mining methods on texts in social research. Refer <<Stulpe_etal2016>> for a detailed explanation.",
                     parent=method1)
    db.session.add(method2)
    method2 = Method(id=57,name="new forms of digital research design",
                     description="New possibilities in surveys or data aquisition techniques.",
                     parent=method1)
    db.session.add(method2)
    method3 = Method(id=58,name="wiki surveys",
                     description="Guide open-answer questions with user feedback. Refer also (<<Salganik2018>>,111)",
                     parent=method2)
    db.session.add(method3)
    m_online_experiment = Method(id=59,name="online experiments",
                     description="Synchronous or asynchronous online experiments.",
                     parent=method2)
    db.session.add(m_online_experiment)
    method3 = Method(id=60,name="survey data linked to big data sources",
                     description="",
                     parent=method2)
    db.session.add(method3)
    method4 = Method(id=61,name="enriched asking",
                     description="'In enriched asking, survey data build context around a big data source that contains some important measurements but lacks others.'(<<Salganik2018>>,118)",
                     parent=method3)
    db.session.add(method4)
    method4 = Method(id=62,name="amplified asking",
                     description="'Amplified asking using a predictive model to combine survey data from few people with a big data source from many people.'(<<Salganik2018>>,122)",
                     parent=method3)
    db.session.add(method4)
    method2 = Method(id=63,name="collaborative work",
                     description="",
                     parent=method1)
    db.session.add(method2)
    method3 = Method(id=64,name="open call projects",
                     description="(e.g. annotation).",
                     parent=method2)
    db.session.add(method3)
    method3 = Method(id=65,name="distributed data collection",
                     description="",
                     parent=method2)
    db.session.add(method3)
    method2 = Method(id=66,name="digital communication",
                     description="",
                     parent=method1)
    db.session.add(method2)
    method2 = Method(id=67,name="digital data/phenomena as reasearch-objective",
                     description="",
                     parent=method1)
    db.session.add(method2)
    m_statistical_modeling = Method(id=68,name="statistical modeling",
                     description="",
                     parent=method1)
    db.session.add(m_statistical_modeling)
    m_regression_analysis = Method(id=69,name="regression analysis",
                     description="",
                     parent=m_statistical_modeling)
    db.session.add(m_regression_analysis)
    m_time_series_analysis = Method(id=70,name="time-series analysis",
                     description="",
                     parent=m_statistical_modeling)
    db.session.add(m_time_series_analysis)
    m_nowcasting = Method(id=71,name="forecasting/nowcasting",
                     description="Using methods to predict the future for estimation of current values. (Example: predict influenza epidemiology combining CDC Data and Google Trends(<<Salganik2018>>,46–50)).",
                     parent=m_time_series_analysis)
    db.session.add(m_nowcasting)
    m_social_complexity = Method(id=72,name="social complexity modeling/ social simulation",
                     description="",
                     parent=method1)
    db.session.add(m_social_complexity)
    m_agent_based_modeling = Method(id=73,name="agent-based modeling",
                     description="",
                     parent=m_social_complexity)
    db.session.add(m_agent_based_modeling)

    reference = Reference(name="Rogers2013",
                          cited="Rogers, R. (2013). Digital methods. Cambridge, Massachusetts, London, England: The MIT Press.")
    db.session.add(reference)
    reference = Reference(name="Jünger2018",
                          cited="Jünger, Jakob (2018): Mapping the Field of Automated Data Collection on the Web. Data Types, Collection Approaches and their Research Logic. In: Stützer, Cathleen / Welker, Martin / Egger, Marc (Hg). Computational Social Science in the Age of Big Data. Concepts, Methodologies, Tools, and Applications. Neue Schriften zur Online-Forschung der Deutschen Gesellschaft für Online-Forschung (DGOF). Köln: Halem-Verlag, S. 104-130.")
    db.session.add(reference)
    reference = Reference(name="Han_etal2012",
                          cited="Han, J., Kamber, M., & Pei, J. (2012). Data Mining: Concepts and Techniques. Saint Louis, UNITED STATES: Elsevier Science & Technology.")
    db.session.add(reference)
    reference = Reference(name="Ignatow_etal2017",
                          cited="Ignatow, G., & Mihalcea, R. F. (2017). Text mining: A guidebook for the social sciences. Los Angeles, London, New Delhi, Singapore, Washington DC, Melbourne: Sage.")
    db.session.add(reference)
    reference = Reference(name="Wickham_etal2017",
                          cited="Wickham, H., & Grolemund, G. (2017). R for Data Science: Import, tidy, transform, visualize, and model data. Beijing, Boston, Farnham, Sebastopol, Tokyo: O’Reilly UK Ltd.")
    db.session.add(reference)
    reference = Reference(name="Papilloud_etal2018",
                          cited="Papilloud, C., & Hinneburg, A. (Eds.). (2018). Studienskripten zur Soziologie. Qualitative Textanalyse mit Topic-Modellen: Eine Einführung für Sozialwissenschaftler. Wiesbaden: Springer VS.")
    db.session.add(reference)
    reference = Reference(name="Maier_etal2018",
                          cited="Maier, D., Waldherr, A., Miltner, P., Wiedemann, G., Niekler, A., Keinert, A., . . . Adam, S. (2018). Applying LDA Topic Modeling in Communication Research: Toward a Valid and Reliable Methodology. Communication Methods and Measures, 12(2-3), 93–118. https://doi.org/10.1080/19312458.2018.1430754")
    db.session.add(reference)
    reference = Reference(name="Roberts2013",
                          cited="Roberts, M. E., Stewart, B. M., Tingley, D., Airoldi, E. M., & others (2013). The structural topic model and applied social science. In Advances in neural information processing systems workshop on topic models: computation, application, and evaluation (pp. 1–20).")
    db.session.add(reference)
    reference = Reference(name="Salganik2018",
                          cited="Salganik, M. J. (2018). Bit by bit: Social research in the digital age.")
    db.session.add(reference)
    reference = Reference(name="Cabrio2018",
                          cited="Cabrio, E., & Villata, S. (2018). Five years of argument mining: a data-driven analysis. In Proceedings of the 27th International Joint Conference on Artificial Intelligence (pp. 5427–5433).")
    db.session.add(reference)
    reference = Reference(name="Stab_etal2018",
                          cited="Stab, C., Daxenberger, J., Stahlhut, C., Miller, T., Schiller, B., Tauchmann, C., . . . Gurevych, I. (2018). ArgumenText: Searching for Arguments in Heterogeneous Sources. In Proceedings of the 2018 Conference of the North American Chapter of the Association for Computational Linguistics: Demonstrations (pp. 21–25).")
    db.session.add(reference)
    reference = Reference(name="Niekler_etal2018",
                          cited="Niekler, A., Bleier, A., Kahmann, C., Posch, L., Wiedemann, G., Erdogan, K., . . . Strohmaier, M. (2018). ILCM - A Virtual Research Infrastructure for Large-Scale Qualitative Data. In Proceedings of the Eleventh International Conference on Language Resources and Evaluation (LREC-2018). European Language Resource Association. Retrieved from http://aclweb.org/anthology/L18-1209")
    db.session.add(reference)
    reference = Reference(name="Lemke_etal2016",
                          cited="Lemke, M., & Wiedemann, G. (Eds.). (2016). Text Mining in den Sozialwissenschaften: Grundlagen und Anwendungen zwischen qualitativer und quantitativer Diskursanalyse. Wiesbaden: Springer VS.")
    db.session.add(reference)
    reference = Reference(name="Stulpe_etal2016",
                          cited="Stulpe, A., & Lemke, M. (2016). Blended Reading. In Text Mining in den Sozialwissenschaften (pp. 17–61). Springer.")
    db.session.add(reference)
    reference = Reference(name="Günther_etal2014",
                          cited="Günther E., Scharkow M. (2014). Automatisierte Datenbereinigung bei Inhalts- und Linkanalysen von Online-Nachrichten, in: K. Sommer, M. Wettstein, W. Wirth, & J. Matthes (Hrsg.): Automatisierung der Inhaltsanalyse, Köln: Halem, 111 - 126")
    db.session.add(reference)
    reference = Reference(name="Pomikálek2011",
                          cited="J. Pomikálek (2011). Removing Boilerplate and Duplicate Content from Web Corpora. PhD thesis, Masaryk University, Brno, 2011.")
    db.session.add(reference)



    lic_unknown = License(name="Unknown")
    lic_bsd = License(name="BSD")
    lic_gpl2 = License(name="GPL", version="2.0")
    lic_gpl3 = License(name="GPL", version="3.0")
    lic_agpl3 = License(name="AGPL", version="3.0")
    lic_lgpl = License(name="LGPL")
    lic_apache2 = License(name="Apache", version="2.0")
    lic_mit = License(name="MIT")
    lic_byncnd3 = License(name="CC BY-NC-ND", version="3.0")
    lic_ccdl = License(name="CCDL", version="1.0")
    lic_prop = License(name="Proprietary")

    lang_de = Language(name="german")
    lang_en = Language(name="english")

    os_win = Operatingsystem(name="windows")
    os_browser = Operatingsystem(name="browser")
    os_docker = Operatingsystem(name="docker")
    os_ix = Operatingsystem(name="linux")
    os_osx = Operatingsystem(name="osx")
    os_ios = Operatingsystem(name="ios")
    os_droid = Operatingsystem(name="android")

    prol_r = Programminglanguage(name="R")
    prol_py = Programminglanguage(name="Python")
    prol_cy = Programminglanguage(name="Cython")
    prol_java = Programminglanguage(name="Java")
    prol_scala = Programminglanguage(name="Scala")
    prol_objc = Programminglanguage(name="Objective-C")
    prol_jupyternb = Programminglanguage(name="Jupyter Notebook")
    prol_js = Programminglanguage(name="Javascript")
    prol_c = Programminglanguage(name="C")
    prol_csharp = Programminglanguage(name="C#")
    prol_ruby = Programminglanguage(name="Ruby")
    prol_perl = Programminglanguage(name="Perl")

    cat_tracking = SoftwareCategory(name="user-consented tracking", short_description="Collection of sensor data on (mobile) devices in accordance with data protection laws.")

    cat_scraping = SoftwareCategory(name="general crawling/scraping", short_description="Tools in the area of web-crawling and scraping.")
    cat_scraping_social_platforms = SoftwareCategory(name="scraping social platforms", short_description="Tools for specific social (media) platforms.")
    cat_scraping_doc = SoftwareCategory(name="scraping documents", short_description="Tools for extracting semantic content out of documents (e.g pdfs, ...)")
    cat_scraping_browser_based = SoftwareCategory(name="crawling/scraping by automating web browsers", short_description="Scraping with remote controlled browser-engines.")
    cat_scraping_html = SoftwareCategory(name="scraping websites", short_description="Tools for extraction of text body in html websites.")

    cat_int = SoftwareCategory(name="tools for corpus linguistics/text mining/(semi-)automated text analysis", short_description="Integrated platforms for corpus analysis and processing.")
    cat_qda = SoftwareCategory(name="computer assisted/aided qualitative data analysis software (CAQDAS)", short_description="assist with qualitative research such as transcription analysis, coding and text interpretation, recursive abstraction, content analysis, discourse analysis, grounded theory methodology, etc.")
    cat_tm = SoftwareCategory(name="natuaral language processing(NLP)", short_description="")
    cat_senti = SoftwareCategory(name="sentiment analysis", short_description="")
    cat_topic = SoftwareCategory(name="topic-models", short_description="")
    cat_visu = SoftwareCategory(name="visualization", short_description="")
    cat_kollab_anno = SoftwareCategory(name="collaborative annotation/writing", short_description="")
    cat_stat = SoftwareCategory(name="statistical software", short_description="software that helps calcualting with specific statistical models")
    cat_repo = SoftwareCategory(name="research data archiving", short_description="")
    cat_now = SoftwareCategory(name="nowcasting", short_description="")
    cat_net = SoftwareCategory(name="network analysis", short_description="social network analysis")
    cat_esmema = SoftwareCategory(name="ESM/EMA surveys", short_description="Datenerhebung in 'natürlicher' Umgebung.")
    cat_transkript = SoftwareCategory(name="audio-transcriptions", short_description="software that converts speech into electronic text document.")
    cat_search = SoftwareCategory(name="search", short_description="information retrieval in large datasets.")
    cat_ocr = SoftwareCategory(name="optical character recognition (OCR)",short_description="OCR is the mechanical or electronic conversion of images of typed, handwritten or printed text into machine-encoded text.")
    cat_oe = SoftwareCategory(name="online experiments", short_description="")
    cat_agent = SoftwareCategory(name="agent-based modeling", short_description="")
    cat_jour = SoftwareCategory(name="investigative journalism", short_description="")
    cat_eye = SoftwareCategory(name="(remote) eye tracking")
    cat_misc = SoftwareCategory(name="miscellaneous", short_description="")

    tool = Software(name="Scrapy",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_scraping,
                    architecture="framework",
                    license=lic_bsd,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://scrapy.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/scrapy/scrapy", comment=""))

    tool = Software(name="Beautiful Soup",
                    short_description="'Beautiful Soup is a Python library designed for quick turnaround projects like screen-scraping. Three features make it powerful: Beautiful Soup provides a few simple methods and Pythonic idioms for navigating, searching, and modifying a parse tree: a toolkit for dissecting a document and extracting what you need. It doesn't take much code to write an application;Beautiful Soup automatically converts incoming documents to Unicode and outgoing documents to UTF-8. You don't have to think about encodings, unless the document doesn't specify an encoding and Beautiful Soup can't detect one. Then you just have to specify the original encoding.;Beautiful Soup sits on top of popular Python parsers like lxml and html5lib, allowing you to try out different parsing strategies or trade speed for flexibility.'link:https://www.crummy.com/software/BeautifulSoup/[Retrieved 22.03.2019]",
                    developer="Leonard Richardson",
                    maintainer="Leonard Richardson",
                    softwarecategory=cat_scraping,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.crummy.com/software/BeautifulSoup/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/scrapy/scrapy", comment=""))

    tool = Software(name="Rcrawler",
                    short_description="'RCrawler is a contributed R package for domain-based web crawling and content scraping. As the first implementation of a parallel web crawler in the R environment, RCrawler can crawl, parse, store pages, extract contents, and produce data that can be directly employed for web content mining applications.' link:https://www.sciencedirect.com/science/article/pii/S2352711017300110[Retrieved 22.03.2019]",
                    developer="Salim Khalil",
                    maintainer="Salim Khalil",
                    softwarecategory=cat_scraping,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.sciencedirect.com/science/article/pii/S2352711017300110", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/salimk/Rcrawler", comment=""))

    tool = Software(name="Robobrowser",
                    short_description="",
                    developer="Joshua Carp",
                    maintainer="Joshua Carp",
                    softwarecategory=cat_scraping,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://robobrowser.readthedocs.io/en/latest/readme.html", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/jmcarp/robobrowser", comment=""))

    tool = Software(name="rvest",
                    short_description="",
                    developer="Hadley Wickham",
                    maintainer="Hadley Wickham",
                    softwarecategory=cat_scraping,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/web/packages/rvest/index.html", comment="cran"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/tidyverse/rvest", comment=""))

    tool = Software(name="facepager",
                    short_description="",
                    developer="Jakob Jünger and Till Keyling",
                    maintainer="Jakob Jünger",
                    softwarecategory=cat_scraping_social_platforms,
                    architecture="stand-alone application",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="wiki", url="https://github.com/strohne/Facepager", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/strohne/Facepager", comment=""))

    tool = Software(name="TWINT",
                    short_description="TWINT (Twitter Intelligence Tool) 'Formerly known as Tweep, Twint is an advanced Twitter scraping tool written in Python that allows for scraping Tweets from Twitter profiles without using Twitter's API.' link:https://github.com/twintproject/twint[Retrieved 07.03.2019]",
                    developer="Cody Zacharias",
                    maintainer="Cody Zacharias",
                    softwarecategory=cat_scraping_social_platforms,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://twint.io/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/twintproject/twint", comment=""))

    tool = Software(name="YouTubeComments",
                    short_description="'This repository contains an R script as well as an interactive Jupyter notebook to demonstrate how to automatically collect, format, and explore YouTube comments, including the emojis they contain. The script and notebook showcase the following steps: Getting access to the YouTube API Extracting comments for a video Formatting the comments & extracting emojis Basic sentiment analysis for text & emojis' link:https://github.com/JuKo007/YouTubeComments[Retrieved 07.03.2019]",
                    developer="Kohne, J., Breuer, J., & Mohseni, M. R.",
                    maintainer="Kohne, J., Breuer, J., & Mohseni, M. R.",
                    softwarecategory=cat_scraping_social_platforms,
                    architecture="library",
                    license=lic_unknown,
                    programminglanguages=[prol_r],
                    price="0",
                    recommandedcitation="Kohne, J., Breuer, J., & Mohseni, M. R. (2018). Automatic Sampling and Analysis of YouTube Comments. doi:10.17605/OSF.IO/HQSXE")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://osf.io/hqsxe/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/JuKo007/YouTubeComments", comment=""))

    tool = Software(name="youtube-transcript-scraper",
                    short_description="Since YouTube does not provide automatically generated transcripts via its API and normal scraping does not work with YT's ajaxy interface, this script uses browser automation to click through the YouTube web interface and download the transcript file.",
                    developer="Bernhard Rieder",
                    maintainer="Bernhard Rieder",
                    softwarecategory=cat_scraping_social_platforms,
                    architecture="library",
                    license=lic_unknown,
                    programminglanguages=[prol_py],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/bernorieder/youtube-transcript-scraper", comment=""))

    tool = Software(name="Grobid",
                    short_description="GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured XML/TEI encoded documents with a particular focus on technical and scientific publications.",
                    developer="Patrice Lopez",
                    maintainer="Patrice Lopez",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_java],
                    price="0",
                    recommandedcitation="@misc{GROBID, title = {GROBID}, howpublished = {\\url{https://github.com/kermitt2/grobid}}, publisher = {GitHub}, year = {2008 --- 2019}, archivePrefix = {swh}, eprint = {1:dir:6a298c1b2008913d62e01e5bc967510500f80710}}")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://cloud.science-miner.com/grobid/", comment="demo"))
    db.session.add(Link(software=tool, type="website", url="https://grobid.readthedocs.io/en/latest/", comment="documentation"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/kermitt2/grobid", comment=""))

    tool = Software(name="LexisNexisTools",
                    short_description="This package provides functions to read files manually downloaded from ‘LexisNexis’ and comes with a few other features I think come in handy while working with data from the popular newspaper archive.",
                    developer="Johannes Gruber",
                    maintainer="Johannes Gruber",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/web/packages/LexisNexisTools/index.html", comment="cran"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/JBGruber/LexisNexisTools", comment=""))

    tool = Software(name="news_extract",
                    short_description="news_extract allows the output of the NexisUni and Factiva databases to be imported into Python. Note, you must export your documents manually first! This module does not scrape the databases directly; rather, it extracts articles and associated metadata from pre-exported output files.",
                    developer="Deen Freelon",
                    maintainer="Deen Freelon",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/dfreelon/news_extract", comment=""))

    tool = Software(name="pdf2xml",
                    short_description="convert PDF files to XML. This script heavily relies on Apache Tika and pdftotext for the extraction of text and the conversion to XML. It tries to combine information from both tools and different conversion modes:",
                    developer="Jörg Tiedemann",
                    maintainer="Jörg Tiedemann",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_perl, prol_java],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://bitbucket.org/tiedemann/pdf2xml/src/master/", comment=""))

    tool = Software(name="PdfAct",
                    short_description="A basic tool that extracts the structure from the PDF files of scientific articles.",
                    developer="Claudius Korzen",
                    maintainer="Claudius Korzen",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_java],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/ad-freiburg/pdfact", comment=""))

    tool = Software(name="pdfalto",
                    short_description="pdfalto is a command line executable for parsing PDF files and producing structured XML representations of the PDF content in ALTO format.",
                    developer="Hervé Déjean (XRCE)",
                    maintainer="Patrice Lopez",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl2,
                    programminglanguages=[prol_c],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/kermitt2/pdfalto", comment=""))

    tool = Software(name="PDFBox",
                    short_description="The Apache PDFBox® library is an open source Java tool for working with PDF documents. This project allows creation of new PDF documents, manipulation of existing documents and the ability to extract content from documents.",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_java],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://pdfbox.apache.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://svn.apache.org/viewvc/pdfbox/", comment=""))

    tool = Software(name="PDFMiner.six",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=None,
                    programminglanguages=[],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/pdfminer/pdfminer.six", comment=""))

    tool = Software(name="poppler",
                    short_description="A library for rendering PDF files, and examining or modifying their structure.",
                    developer="Derek Noonburg and others",
                    maintainer="",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_c],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://poppler.freedesktop.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://gitlab.freedesktop.org/poppler/poppler", comment=""))

    tool = Software(name="readtext",
                    short_description="readtext is a one-function package that does exactly what it says on the tin: It reads files containing text, along with any associated document-level metadata, which we call “docvars”, for document variables. Plain text files do not have docvars, but other forms such as .csv, .tab, .xml, and .json files usually do.",
                    developer="Kenneth Benoit [aut, cre, cph], Adam Obeng [aut], Kohei Watanabe [ctb], Akitaka Matsuo [ctb], Paul Nulty [ctb], Stefan Müller [ctb]",
                    maintainer="Kenneth Benoit",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://readtext.quanteda.io/reference/readtext.html", comment=""))
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/package=readtext", comment="cran"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/quanteda/readtext", comment=""))

    tool = Software(name="xpdf",
                    short_description="Xpdf is a free PDF viewer and toolkit, including a text extractor, image converter, HTML converter, and more. Most of the tools are available as open source.",
                    developer="Derek Noonburg",
                    maintainer="Derek Noonburg",
                    softwarecategory=cat_scraping_doc,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_c],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="http://www.xpdfreader.com/about.html", comment=""))

    tool = Software(name="Puppeteer",
                    short_description="Puppeteer is a Node library which provides a high-level API to control Chrome or Chromium over the DevTools Protocol. Puppeteer runs headless by default, but can be configured to run full (non-headless) Chrome or Chromium.",
                    developer="Chrome DevTools team",
                    maintainer="Chrome DevTools team",
                    softwarecategory=cat_scraping_browser_based,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_js],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://pptr.dev/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/GoogleChrome/puppeteer", comment=""))

    tool = Software(name="Pyppeteer",
                    short_description="Unofficial Python port of puppeteer JavaScript (headless) chrome/chromium browser automation library.",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_scraping_browser_based,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/miyakogi/pyppeteer", comment=""))
    db.session.add(Link(software=tool, type="website", url="https://miyakogi.github.io/pyppeteer/", comment="documentation"))

    tool = Software(name="RSelenium",
                    short_description="",
                    developer="John Harrison",
                    maintainer="Ju Yeong Kim",
                    softwarecategory=cat_scraping_browser_based,
                    architecture="library",
                    license=lic_agpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/ropensci/RSelenium", comment=""))

    tool = Software(name="Selenium",
                    short_description="Selenium is an umbrella project encapsulating a variety of tools and libraries enabling web browser automation. Selenium specifically provides infrastructure for the W3C WebDriver specification — a platform and language-neutral coding interface compatible with all major web browsers.",
                    developer="Jason Huggins",
                    maintainer="",
                    softwarecategory=cat_scraping_browser_based,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_java],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://docs.seleniumhq.org", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/SeleniumHQ/selenium", comment=""))

    tool = Software(name="splash",
                    short_description="Splash is a javascript rendering service with an HTTP API. It's a lightweight browser with an HTTP API, implemented in Python 3 using Twisted and QT5.",
                    developer="Scrapinghub",
                    maintainer="Scrapinghub",
                    softwarecategory=cat_scraping_browser_based,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://splash.readthedocs.io/en/stable/", comment="documentation"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/scrapinghub/splash", comment=""))

    tool = Software(name="boilerpipeR",
                    short_description="Generic Extraction of main text content from HTML files; removal of ads, sidebars and headers using the boilerpipe (http://code.google.com/p/boilerpipe/) Java library. The extraction heuristics from boilerpipe show a robust performance for a wide range of web site templates.",
                    developer="C. Kohlschütter,P.Fankhauser,W.Nejdl",
                    maintainer="Mario Annau",
                    softwarecategory=cat_scraping_html,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/web/packages/boilerpipeR/index.html", comment="cran"))

    tool = Software(name="goose3",
                    short_description="'Goose was originally an article extractor written in Java that has most recently (Aug2011) been converted to a scala project. This is a complete rewrite in Python. The aim of the software is to take any news article or article-type web page and not only extract what is the main body of the article but also all meta data and most probable image candidate.'",
                    developer="Xavier Grangier",
                    maintainer="Xavier Grangier",
                    softwarecategory=cat_scraping_html,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/goose3/goose3", comment=""))

    tool = Software(name="jusText",
                    short_description="'jusText is a tool for removing boilerplate content, such as navigation links, headers, and footers from HTML pages. It is designed to preserve mainly text containing full sentences and it is therefore well suited for creating linguistic resources such as Web corpora.'",
                    developer="Jan Pomikálek",
                    maintainer="",
                    softwarecategory=cat_scraping_html,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://corpus.tools/wiki/Justext", comment=""))

    tool = Software(name="newspaper3k",
                    short_description="Newspaper delivers Instapaper style article extraction.",
                    developer="Lucas Ou-Yang",
                    maintainer="Lucas Ou-Yang",
                    softwarecategory=cat_scraping_html,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://newspaper.readthedocs.io/en/latest/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/codelucas/newspaper", comment=""))

    tool = Software(name="python-boilerpipe",
                    short_description="A python wrapper for Boilerpipe, an excellent Java library for boilerplate removal and fulltext extraction from HTML pages.",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_scraping_html,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/misja/python-boilerpipe", comment=""))

    tool = Software(name="AWARE",
                    short_description="'AWARE is an Android framework dedicated to instrument, infer, log and share mobile context information, for application developers, researchers and smartphone users. AWARE captures hardware-, software-, and human-based data. The data is then analyzed using AWARE plugins. They transform data into information you can understand.' link:http://www.awareframework.com/what-is-aware/[Source, visited: 27.02.2019]",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tracking,
                    architecture="framework",
                    license=lic_apache2,
                    programminglanguages=[prol_java],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://www.awareframework.com/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/denzilferreira/aware-client", comment="android"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/tetujin/aware-client-ios", comment="iOS"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/tetujin/aware-client-osx", comment="OSX"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/denzilferreira/aware-server", comment="server"))

    tool = Software(name="Filter-Bubble-Web-Historian",
                    short_description="Part of this project makes use of the Web Historian browser extension, developed by Ericka Menchen-Trevino. To configure this extension, the files contained in this repository can be combined with the Web Historian repository, which is added as a submodule.",
                    developer="Ericka Menchen-Trevino and Chris Karr",
                    maintainer="Laurens Bogaardt",
                    softwarecategory=cat_tracking,
                    architecture="plugin",
                    license=lic_gpl3,
                    programminglanguages=[prol_py, prol_js],
                    languages=[lang_en],
                    operatingsystems=[os_browser],
                    currentversion="1.0",
                    lastchanged=datetime.datetime.strptime('13092019', '%d%m%Y').date(),
                    methods=[m_esm_ema],
                    price="0",
                    recommandedcitation="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://ccs.amsterdam/projects/jeds/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/Filter-Bubble", comment="all"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/WebHistorian/community", comment=""))

    tool = Software(name="Passive Data Kit",
                    short_description="",
                    developer="Chris Karr",
                    maintainer="Chris Karr",
                    softwarecategory=cat_tracking,
                    architecture="framework",
                    license=lic_apache2,
                    programminglanguages=[prol_py, prol_java],
                    languages=[lang_en],
                    operatingsystems=[os_ix, os_droid, os_ios],
                    currentversion="",
                    lastchanged=datetime.datetime.strptime('26022019', '%d%m%Y').date(),
                    methods=[m_esm_ema],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://passivedatakit.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/audaciouscode/PassiveDataKit-Django", comment="djangoserver"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/audaciouscode/PassiveDataKit-Android", comment="android"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/audaciouscode/PassiveDataKit-iOS", comment="iOS"))

    tool = Software(name="Web Historian(CE)",
                    short_description="Chrome browser extension designed to integrate web browsing history data collection into research projects collecting other types of data from participants (e.g. surveys, in-depth interviews, experiments). It uses client-side D3 visualizations to inform participants about the data being collected during the informed consent process. It allows participants to delete specific browsing data or opt-out of browsing data collection. It directs participants to an online survey once they have reviewed their data and made a choice of whether to participate. It has been used with Qualtrics surveys, but any survey that accepts data from a URL will work. It works with the open source Passive Data Kit (PDK) as the backend for data collection. To successfully upload, you need to fill in the address of your PDK server in the js/app/config.js file.",
                    developer="Ericka Menchen-Trevino and Chris Karr",
                    maintainer="Ericka Menchen-Trevino and Chris Karr",
                    softwarecategory=cat_tracking,
                    architecture="plugin",
                    license=lic_gpl3,
                    programminglanguages=[prol_py, prol_js],
                    languages=[lang_en],
                    operatingsystems=[os_browser],
                    currentversion="e06b3e174f9668f5c62f30a9bedde223023e0bca",
                    lastchanged=datetime.datetime.strptime('18022019', '%d%m%Y').date(),
                    methods=[m_esm_ema],
                    price="0",
                    recommandedcitation="Menchen-Trevino, E., & Karr, C. (2018). Web Historian - Community Edition. Zenodo. https://doi.org/10.5281/zenodo.1322782")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://doi.org/10.5281/zenodo.1322782", comment="doi"))
    db.session.add(Link(software=tool, type="website", url="http://www.webhistorian.org", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/WebHistorian/community", comment=""))

    tool = Software(name="AmCAT",
                    short_description="'The Amsterdam Content Analysis Toolkit (AmCAT) is an open source infrastructure that makes it easy to do large-scale automatic and manual content analysis (text analysis) for the social sciences and humanities.'",
                    developer="Chris Karr",
                    maintainer="Ju Yeong Kim",
                    softwarecategory=cat_int,
                    architecture="server application",
                    license=lic_agpl3,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://vanatteveldt.com/amcat/", comment="entwickler"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/amcat/amcat", comment=""))
    db.session.add(Link(software=tool, type="wiki", url="http://wiki.amcat.nl/3.4:AmCAT_Navigator_3", comment=""))

    tool = Software(name="CorpusExplorer",
                    short_description="'OpenSource Software für Korpuslinguist*innen und Text-/Data-Mining Interessierte. Der CorpusExplorer vereint über 50 interaktiven Auswertungsmöglichkeiten mit einer einfachen Bedienung. Routineaufgaben wie z. B. Textakquise, Taggen oder die grafische Aufbereitung von Ergebnissen werden vollständig automatisiert. Die einfache Handhabung erleichtert den Einsatz in der universitären Lehre und führt zu schnellen sowie gehaltvollen Ergebnissen. Dabei ist der CorpusExplorer offen für viele Standards (XML, CSV, JSON, R, uvm.) und bietet darüber hinaus ein eigenes Software Development Kit (SDK) an, mit dem es möglich ist, alle Funktionen in eigene Programme zu integrieren.' link:https://notes.jan-oliver-ruediger.de/software/corpusexplorer-overview/[source, retrieved 22.03.2019]",
                    developer="Jan Oliver Rüdiger",
                    maintainer="Jan Oliver Rüdiger",
                    architecture="stand-alone application",
                    softwarecategory=cat_int,
                    license=lic_agpl3,
                    programminglanguages=[prol_csharp],
                    languages=[lang_de],
                    operatingsystems=[os_win],
                    currentversion="Q12019",
                    lastchanged=datetime.datetime.strptime('04032019', '%d%m%Y').date(),
                    methods=[m_w_dict_frequencies],
                    price="0",
                    recommandedcitation="Rüdiger, Jan Oliver (2018): CorpusExplorer. Version 2.0. Universität Kassel - Universität Siegen. Online verfügbar unter http://corpusexplorer.de")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://notes.jan-oliver-ruediger.de/software/corpusexplorer-overview/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/notesjor/corpusexplorer2.0", comment=""))

    tool = Software(name="COSMOS",
                    short_description="COSMOS Open Data Analytics software",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_int,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://socialdatalab.net/COSMOS", comment=""))

    tool = Software(name="CWB",
                    short_description="CWB, the IMS[Institut für Maschinelle Sprachverarbeitung Stuttgart] Open Corpus Workbench is 'a fast, powerful and extremely flexible corpus querying system.'",
                    developer="Stefan Evert et al.",
                    maintainer="Stefan Evert et al.",
                    softwarecategory=cat_int,
                    architecture="server application",
                    license=lic_gpl3,
                    programminglanguages=[prol_c,prol_perl],
                    languages=[lang_en],
                    operatingsystems=[os_win,os_ix],
                    currentversion="3.4.15",
                    lastchanged=datetime.datetime.strptime('01032019', '%d%m%Y').date(),
                    methods=[m_search_query,m_indexing,m_frequency_analysis],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://cwb.sourceforge.net/index.php", comment=""))
    db.session.add(Link(software=tool, type="repository", url="http://svn.code.sf.net/p/cwb/code/cwb/trunk", comment="cwb"))
    db.session.add(Link(software=tool, type="repository", url="http://svn.code.sf.net/p/cwb/code/gui/cqpweb/trunk", comment="cqpweb"))

    tool = Software(name="LCM",
                    short_description="Leipzig Corpus Miner a decentralized SaaS application for the analysis of very large amounts of news texts ",
                    developer="Gregor Wiedeman, Andreas Niekler",
                    maintainer="Gregor Wiedeman, Andreas Niekler",
                    softwarecategory=cat_int,
                    architecture="framework",
                    license=lic_lgpl,
                    programminglanguages=[prol_java,prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://lcm.informatik.uni-leipzig.de/generic.html", comment=""))

    tool = Software(name="iLCM",
                    short_description="'The iLCM[LCM=Leipzig Corpus Miner] project pursues the development of an integrated research environment for the analysis of structured and unstructured data in a ‘Software as a Service’ architecture (SaaS). The research environment addresses requirements for the quantitative evaluation of large amounts of qualitative data using text mining methods and requirements for the reproducibility of data-driven research designs in the social sciences.' link:http://ilcm.informatik.uni-leipzig.de/ilcm/ilcm/[source, retrieved 08.03.2019]",
                    developer="Andreas Niekler, Arnim Bleier, Christian Kahmann, Lisa Posch, Gregor Wiedemann, Kenan Erdogan, Gerhard Heyer and Markus Strohmaier",
                    maintainer="Andreas Niekler, Arnim Bleier, Christian Kahmann, Lisa Posch, Gregor Wiedemann, Kenan Erdogan, Gerhard Heyer and Markus Strohmaier",
                    architecture="server application",
                    softwarecategory=cat_int,
                    license=lic_lgpl,
                    programminglanguages=[prol_java,prol_py,prol_r],
                    languages=[lang_de],
                    operatingsystems=[os_browser,os_docker],
                    currentversion="0.96",
                    lastchanged=datetime.datetime.strptime('05032019', '%d%m%Y').date(),
                    methods=[m_text_preprocessing, m_pos_tagging, m_syntactic_parsing, m_context_volatility, m_co_occurence, m_w_dict_frequencies, lda],
                    price="0",
                    recommandedcitation="Niekler, A., Bleier, A., Kahmann, C., Posch, L., Wiedemann, G., Erdogan, K., Heyer, G., & Strohmaier, M. (2018). iLCM - A Virtual Research Infrastructure for Large-Scale Qualitative Data. CoRR, abs/1805.11404.")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://ilcm.informatik.uni-leipzig.de/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://hub.docker.com/r/ckahmann/ilcm_r/tags", comment="docker"))

    tool = Software(name="ATLAS.ti",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[],
                    price="600€/1 User/Perpetual")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://atlasti.com/de/produkt/what-is-atlas-ti/", comment=""))

    tool = Software(name="Leximancer",
                    short_description="'Leximancer automatically analyses your text documents to identify the high level concepts in your text documents, delivering the key ideas and actionable insights you need with powerful interactive visualisations and data exports.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[],
                    price="1500 AUD/? User/Perpetual")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://info.leximancer.com/", comment=""))

    tool = Software(name="MAXQDA",
                    short_description="'MAXQDA gehört zu den weltweit führenden und umfangreichsten QDA-Software-Programmen im Bereich der Qualitativen und Mixed-Methods-Forschung. Die Software hilft Ihnen beim Erfassen, Organisieren, Analysieren, Visualisieren und Veröffentlichen Ihrer Daten. Ob Grounded Theory, Literaturreview, explorative Marktforschung, Interviews, Webseitenanalyse oder Surveys: Analysieren Sie was Sie wollen, wie Sie wollen.MAXQDA Analytics Pro ist die erweiterte Version von MAXQDA und enthält neben allen Funktionen für die Qualitative & Mixed Methods-Forschung auch ein Modul für die quantitative Textanalyse (MAXDictio) und ein Modul für die statistische Datenanalyse (MAXQDA Stats)'link:https://www.rrz.uni-hamburg.de/services/software/alphabetisch/maxqda.html[Source, visited: 27.02.2019]",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[],
                    price="Unilizenz")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.rrz.uni-hamburg.de/services/software/alphabetisch/maxqda.html", comment="uhh"))

    tool = Software(name="NVivo",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[],
                    price="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.qsrinternational.com/nvivo/who-uses-nvivo/academics", comment=""))

    tool = Software(name="QDAMiner",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://provalisresearch.com/products/qualitative-data-analysis-software/", comment=""))

    tool = Software(name="ORA Pro",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://netanomics.com/", comment=""))

    tool = Software(name="Quirkos",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.quirkos.com/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="RQDA",
                    short_description="'It includes a number of standard Computer-Aided Qualitative Data Analysis features. In addition it seamlessly integrates with R, which means that a) statistical analysis on the coding is possible, and b) functions for data manipulation and analysis can be easily extended by writing R functions. To some extent, RQDA and R make an integrated platform for both quantitative and qualitative data analysis.'",
                    developer="Ronggui Huang",
                    maintainer="Ronggui Huang",
                    softwarecategory=cat_qda,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_r],
                    price="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://rqda.r-forge.r-project.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/Ronggui/RQDA", comment=""))

    tool = Software(name="TAMS",
                    short_description="'Text Analysis Markup System (TAMS) is both a system of marking documents for qualitative analysis and a series of tools for mining information based on that syntax.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_qda,
                    architecture="stand-alone application",
                    license=lic_gpl2,
                    programminglanguages=[],
                    price="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://sourceforge.net/projects/tamsys", comment=""))

    tool = Software(name="Apache OpenNLP",
                    short_description="'OpenNLP supports the most common NLP tasks, such as tokenization, sentence segmentation, part-of-speech tagging, named entity extraction, chunking, parsing, language detection and coreference resolution.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    license=lic_apache2,
                    architecture="library",
                    programminglanguages=[prol_java],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://opennlp.apache.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="GATE",
                    short_description="GATE - General Architecture for Text Engineering",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="framework",
                    license=lic_lgpl,
                    programminglanguages=[prol_java],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://gate.ac.uk/overview.html", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/GateNLP/gate-core", comment=""))

    tool = Software(name="Gensim",
                    short_description="'Gensim is a Python library for topic modelling, document indexing and similarity retrieval with large corpora. Target audience is the natural language processing (NLP) and information retrieval (IR) community.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_lgpl,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://pypi.org/project/gensim/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="koRpus",
                    short_description="'A set of tools to analyze texts.'",
                    developer="Meik Michalke, Earl Brown, Alberto Mirisola, Alexandre Brulet, Laura Hauser",
                    maintainer="Meik Michalke",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://reaktanz.de/?c=hacking&s=koRpus", comment=""))
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/web/packages/koRpus/index.html", comment="cran"))

    tool = Software(name="NLTK",
                    short_description="'NLTK is a leading platform for building Python programs to work with human language data. It provides easy-to-use interfaces to over 50 corpora and lexical resources such as WordNet, along with a suite of text processing libraries for classification, tokenization, stemming, tagging, parsing, and semantic reasoning, wrappers for industrial-strength NLP libraries, and an active discussion forum.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="framework",
                    license=lic_apache2,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://www.nltk.org/index.html", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/nltk/nltk", comment=""))

    tool = Software(name="Pandas",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://pandas.pydata.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/pandas-dev/pandas", comment=""))

    tool = Software(name="polmineR",
                    short_description="",
                    developer="Andreas Blätte",
                    maintainer="Andreas Blätte",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/package=polmineR", comment="cran"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/PolMine/polmineR", comment=""))

    tool = Software(name="quanteda",
                    short_description="'The package is designed for R users needing to apply natural language processing to texts, from documents to final analysis. Its capabilities match or exceed those provided in many end-user software applications, many of which are expensive and not open source. The package is therefore of great benefit to researchers, students, and other analysts with fewer financial resources. While using quanteda requires R programming knowledge, its API is designed to enable powerful, efficient analysis with a minimum of steps. By emphasizing consistent design, furthermore, quanteda lowers the barriers to learning and using NLP and quantitative text analysis even for proficient R programmers.'",
                    developer="Kenneth Benoit",
                    maintainer="Kenneth Benoit",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://quanteda.io/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/quanteda/quanteda", comment=""))

    tool = Software(name="RapidMiner",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="stand-alone application",
                    license=lic_agpl3,
                    programminglanguages=[prol_java],
                    price="")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://rapidminer.com/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/rapidminer/rapidminer-studio", comment=""))

    tool = Software(name="spaCy",
                    short_description="spaCy 'excels at large-scale information extraction tasks. It's written from the ground up in carefully memory-managed Cython. Independent research has confirmed that spaCy is the fastest in the world. If your application needs to process entire web dumps, spaCy is the library you want to be using.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_cy],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://spacy.io/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/explosion/spaCy", comment=""))

    tool = Software(name="Stanford CoreNLP",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_java],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://stanfordnlp.github.io/CoreNLP/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/stanfordnlp/CoreNLP", comment=""))

    tool = Software(name="tm",
                    short_description="",
                    developer="Ingo Feinerer, Kurt Hornik",
                    maintainer="Ingo Feinerer, Kurt Hornik",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://tm.r-forge.r-project.org/", comment=""))
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/package=tm", comment="cran"))

    tool = Software(name="tosca",
                    short_description="'A framework for statistical analysis in content analysis. In addition to a pipeline for preprocessing text corpora and linking to the latent Dirichlet allocation from the 'lda' package, plots are offered for the descriptive analysis of text corpora and topic models. In addition, an implementation of Chang's intruder words and intruder topics is provided.'",
                    developer="Lars Koppers, Jonas Rieger, Karin Boczek, Gerret von Nordheim",
                    maintainer="Lars Koppers",
                    softwarecategory=cat_tm,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/web/packages/tosca/index.html", comment="cran"))

    tool = Software(name="xtas",
                    short_description="the eXtensible Text Analysis Suite(xtas) 'is a collection of natural language processing and text mining tools, brought together in a single software package with built-in distributed computing and support for the Elasticsearch document store.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_tm,
                    architecture="framework",
                    license=lic_apache2,
                    programminglanguages=[prol_py],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://nlesc.github.io/xtas/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/NLeSC/xtas", comment=""))

    tool = Software(name="MALLET",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_topic,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://mallet.cs.umass.edu/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/mimno/Mallet", comment=""))

    tool = Software(name="TOME",
                    short_description="'TOME is a tool to support the interactive exploration and visualization of text-based archives, supported by a Digital Humanities Startup Grant from the National Endowment for the Humanities (Lauren Klein and Jacob Eisenstein, co-PIs). Drawing upon the technique of topic modeling—a machine learning method for identifying the set of topics, or themes, in a document set—our tool allows humanities scholars to trace the evolution and circulation of these themes across networks and over time.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_topic,
                    architecture="stand-alone application",
                    license=lic_unknown,
                    programminglanguages=[prol_py, prol_jupyternb])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://dhlab.lmc.gatech.edu/tome/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/GeorgiaTechDHLab/TOME/", comment=""))

    tool = Software(name="Stm",
                    short_description="'The Structural Topic Model (STM) allows researchers to estimate topic models with document-level covariates. The package also includes tools for model selection, visualization, and estimation of topic-covariate regressions. Methods developed in Roberts et al (2014) <doi:10.1111/ajps.12103> and Roberts et al (2016) <doi:10.1080/01621459.2016.1141684>.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_topic,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_r],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://structuraltopicmodel.com", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/bstewart/stm", comment=""))

    tool = Software(name="lexicoder",
                    short_description="'Lexicoder performs simple deductive content analyses of any kind of text, in almost any language. All that is required is the text itself, and a dictionary. Our own work initially focused on the analysis of newspaper stories during election campaigns, and both television and newspaper stories about public policy issues. The software can deal with almost any text, however, and lots of it. Our own databases typically include up to 100,000 news stories. Lexicoder processes these data, even with a relatively complicated coding dictionary, in about fifteen minutes. The software has, we hope, a wide range of applications in the social sciences. It is not the only software that conducts content analysis, of course - there are many packages out there, some of which are much more sophisticated than this one. The advantage to Lexicoder, however, is that it can run on any computer with a recent version of Java (PC or Mac), it is very simple to use, it can deal with huge bodies of data, it can be called from R as well as from the Command Line, and its free.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_senti,
                    architecture="library",
                    license=lic_prop,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://www.lexicoder.com/index.html", comment=""))

    tool = Software(name="OpinionFinder",
                    short_description="'OpinionFinder is a system that processes documents and automatically identifies subjective sentences as well as various aspects of subjectivity within sentences, including agents who are sources of opinion, direct subjective expressions and speech events, and sentiment expressions. OpinionFinder was developed by researchers at the University of Pittsburgh, Cornell University, and the University of Utah. In addition to OpinionFinder, we are also releasing the automatic annotations produced by running OpinionFinder on a subset of the Penn Treebank.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_senti,
                    architecture="stand-alone application",
                    license=lic_unknown,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://mpqa.cs.pitt.edu/opinionfinder/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="Readme",
                    short_description="'The ReadMe software package for R takes as input a set of text documents (such as speeches, blog posts, newspaper articles, judicial opinions, movie reviews, etc.), a categorization scheme chosen by the user (e.g., ordered positive to negative sentiment ratings, unordered policy topics, or any other mutually exclusive and exhaustive set of categories), and a small subset of text documents hand classified into the given categories.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_senti,
                    architecture="library",
                    license=lic_byncnd3,
                    programminglanguages=[prol_r])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://gking.harvard.edu/readme", comment=""))

    tool = Software(name="Gephi",
                    short_description="'Gephi is an award-winning open-source platform for visualizing and manipulating large graphs.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_visu,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://gephi.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/gephi/gephi/", comment=""))

    tool = Software(name="sigma.js",
                    short_description="'Sigma is a JavaScript library dedicated to graph drawing. It makes easy to publish networks on Web pages, and allows developers to integrate network exploration in rich Web applications.'",
                    developer="Alexis Jacomy",
                    maintainer="Alexis Jacomy",
                    softwarecategory=cat_visu,
                    architecture="library",
                    license=lic_mit,
                    programminglanguages=[prol_js])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://sigmajs.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/jacomyal/sigma.js", comment=""))

    tool = Software(name="scikit-image",
                    short_description="'scikit-image is a collection of algorithms for image processing. It is available free of charge and free of restriction. We pride ourselves on high-quality, peer-reviewed code, written by an active community of volunteers.'",
                    developer="Stéfan van der Walt, Johannes L. Schönberger, Juan Nunez-Iglesias, François Boulogne, Joshua D. Warner, Neil Yager, Emmanuelle Gouillart, Tony Yu, and the scikit-image contributors",
                    maintainer="Stéfan van der Walt, Johannes L. Schönberger, Juan Nunez-Iglesias, François Boulogne, Joshua D. Warner, Neil Yager, Emmanuelle Gouillart, Tony Yu, and the scikit-image contributors",
                    softwarecategory=cat_visu,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://scikit-image.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/scikit-image/scikit-image", comment=""))

    tool = Software(name="CATMA",
                    short_description="'CATMA (Computer Assisted Text Markup and Analysis) is a practical and intuitive tool for text researchers. In CATMA users can combine the hermeneutic, ‘undogmatic’ and the digital, taxonomy based approach to text and corpora—as a single researcher, or in real-time collaboration with other team members.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_kollab_anno,
                    architecture="server application",
                    license=lic_apache2,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://catma.de/", comment=""))
    db.session.add(Link(software=tool, type="website", url="https://www.slm.uni-hamburg.de/germanistik/forschung/forschungsprojekte/catma.html", comment="uhh"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/mpetris/catma", comment=""))

    tool = Software(name="WebAnno",
                    short_description="WebAnno is a multi-user tool supporting different roles such as annotator, curator, and project manager. The progress and quality of annotation projects can be monitored and measuered in terms of inter-annotator agreement. Multiple annotation projects can be conducted in parallel.",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_kollab_anno,
                    architecture="server application",
                    license=lic_apache2,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://webanno.github.io/webanno/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/webanno/webanno", comment=""))

    tool = Software(name="FidusWriter",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_kollab_anno,
                    architecture="server application",
                    license=lic_agpl3,
                    programminglanguages=[prol_py,prol_js])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://fiduswriter.org", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/fiduswriter/fiduswriter", comment=""))

    tool = Software(name="dataverse",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_repo,
                    architecture="server application",
                    license=lic_apache2,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://dataverse.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/IQSS/dataverse", comment=""))

    tool = Software(name="gretl",
                    short_description="Is a cross-platform software package for econometric analysis",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_stat,
                    architecture="stand-alone application",
                    license=lic_gpl3,
                    programminglanguages=[prol_c])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://gretl.sourceforge.net/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://sourceforge.net/p/gretl/git/ci/master/tree/", comment=""))

    tool = Software(name="MLwiN",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_stat,
                    architecture="library",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://www.bristol.ac.uk/cmm/software/mlwin/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="SPSS",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_stat,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.rrz.uni-hamburg.de/services/software/software-thematisch/statistik/spss-netzlizenz.html", comment = "uhh"))


    tool = Software(name="STATA",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_stat,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.rrz.uni-hamburg.de/services/software/software-thematisch/statistik/stata.html", comment = "uhh"))

    tool = Software(name="Nowcasting",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_now,
                    architecture="library",
                    license=lic_gpl3,
                    programminglanguages=[prol_r])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://cran.r-project.org/package=nowcasting", comment="cran"))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/nmecsys/nowcasting", comment=""))

    tool = Software(name="AutoMap",
                    short_description="'AutoMap enables the extraction of information from texts using Network Text Analysis methods. AutoMap supports the extraction of several types of data from unstructured documents. The type of information that can be extracted includes: content analytic data (words and frequencies), semantic network data (the network of concepts), meta-network data (the cross classification of concepts into their ontological category such as people, places and things and the connections among these classified concepts), and sentiment data (attitudes, beliefs). Extraction of each type of data assumes the previously listed type of data has been extracted.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://www.casos.cs.cmu.edu/projects/automap/software.php", comment=""))

    tool = Software(name="NodeXL",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.smrfoundation.org/nodexl/", comment=""))

    tool = Software(name="ORA Pro",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="Pajek",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://mrvar.fdv.uni-lj.si/pajek/", comment=""))

    tool = Software(name="NetworkX",
                    short_description="'Data structures for graphs, digraphs, and multigraphs Many standard graph algorithms Network structure and analysis measures Generators for classic graphs, random graphs, and synthetic networks Nodes can be 'anything' (e.g., text, images, XML records) Edges can hold arbitrary data (e.g., weights, time-series) Open source 3-clause BSD license Well tested with over 90% code coverage Additional benefits from Python include fast prototyping, easy to teach, and multi-platform.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_bsd,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="UCINET",
                    short_description="'UCINET 6 for Windows is a software package for the analysis of social network data. It was developed by Lin Freeman, Martin Everett and Steve Borgatti. It comes with the NetDraw network visualization tool.'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_net,
                    architecture="stand-alone application",
                    license=lic_prop,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://sites.google.com/site/ucinetsoftware/home", comment=""))

    tool = Software(name="LuceneSolr",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_search,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://lucene.apache.org/solr/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    paco = Software(name="paco",
                     short_description="",
                     developer="Bob Evans",
                     maintainer="Bob Evans",
                     softwarecategory=cat_esmema,
                     architecture="framework",
                     license=lic_apache2,
                     programminglanguages=[prol_objc, prol_java])
    db.session.add(paco)
    db.session.add(Link(software=paco, type="website", url="https://www.pacoapp.com/", comment=""))
    db.session.add(Link(software=paco, type="repository", url="https://github.com/google/paco", comment=""))

    f4analyse = Software(name="f4analyse",
                     short_description="",
                     developer="",
                     maintainer="",
                     softwarecategory=cat_transkript,
                     architecture="stand-alone application",
                     license=lic_prop)
    db.session.add(f4analyse)
    db.session.add(Link(software=f4analyse, type="website", url="https://www.audiotranskription.de/f4-analyse", comment=""))

    tool = Software(name="EXMARaLDA",
                    short_description="'EXMARaLDA ist ein System für das computergestützte Arbeiten mit (vor allem) mündlichen Korpora. Es besteht aus einem Transkriptions- und Annotationseditor (Partitur-Editor), einem Tool zum Verwalten von Korpora (Corpus-Manager) und einem Such- und Analysewerkzeug (EXAKT).'",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_transkript,
                    architecture="stand-alone application",
                    license=lic_unknown,
                    programminglanguages=[prol_java])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://exmaralda.org/de/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/EXMARaLDA/exmaralda", comment=""))

    tool = Software(name="tesseract",
                    short_description="'Tesseract is an open source text recognizer (OCR) Engine, available under the Apache 2.0 license. It can be used directly, or (for programmers) using an API to extract printed text from images. It supports a wide variety of languages.'",
                    developer="Google, HP Inc.",
                    maintainer="Ray Smith u. a. ",
                    softwarecategory=cat_ocr,
                    architecture="library",
                    license=lic_apache2,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="repository", url="https://github.com/tesseract-ocr/tesseract", comment=""))

    tool = Software(name="LIONESS",
                    short_description="'LIONESS Lab is a free web-based platform for online interactive experiments. It allows you to develop, test and conduct decision-making experiments with live feedback between participants. LIONESS experiments include a standardized set of methods to deal with the set of challenges arising when conducting interactive experiments online. These methods reflect current ‘best practices’ for, e.g., preventing participants to enter a session more than once, facilitating on-the-fly formation of interaction groups, reducing waiting times for participants, driving down attrition by retaining attention of online participants and, importantly, adequate handling of cases in which participants drop out.With LIONESS Lab you can readily develop and test your experiments online in a user-friendly environment. You can develop experiments from scratch in a point-and-click fashion or start from an existent design from our growing repository and adjust it according your own requirements.' link:https://lioness-lab.org/faq/[Retrieved 07.03.2019]",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_oe,
                    architecture="server application",
                    license=lic_prop,
                    programminglanguages=[prol_js],
                    price="0")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://lioness-lab.org/", comment=""))

    tool = Software(name="nodeGame",
                    short_description="'NodeGame is a free, open source JavaScript/HTML5 framework for conducting synchronous experiments online and in the lab directly in the browser window. It is specifically designed to support behavioral research along three dimensions: larger group sizes, real-time (but also discrete time) experiments, batches of simultaneous experiments.'",
                    developer="Stefan Balietti",
                    maintainer="Stefan Balietti",
                    softwarecategory=cat_oe,
                    architecture="server application",
                    license=lic_mit,
                    programminglanguages=[prol_js],
                    languages=[lang_en],
                    operatingsystems=[os_browser],
                    currentversion="4.2.1",
                    lastchanged=datetime.datetime.strptime('10122018', '%d%m%Y').date(),
                    methods=[m_online_experiment],
                    price="0",
                    recommandedcitation='Balietti (2017) "nodeGame: Real-Time, Synchronous, Online Experiments in the Browser" Behavior Research Methods Volume 49, Issue 5, pp. 1696-1715.')
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://nodegame.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/nodeGame", comment=""))

    tool = Software(name="Breadboard",
                    short_description="'Breadboard is a software platform for developing and conducting human interaction experiments on networks. It allows researchers to rapidly design experiments using a flexible domain-specific language and provides researchers with immediate access to a diverse pool of online participants.' link:http://breadboard.yale.edu/[Retrieved: 07.03.2019]",
                    developer="McKnight, Mark E., and Nicholas A. Christakis",
                    maintainer="McKnight, Mark E., and Nicholas A. Christakis",
                    softwarecategory=cat_oe,
                    architecture="server application",
                    license=lic_unknown,
                    programminglanguages=[prol_js],
                    price="0",
                    recommandedcitation="McKnight, Mark E., and Nicholas A. Christakis. Breadboard. Computer software. Breadboard: Software for Online Social Experiments. Vers. 2. Yale University, 1 May 2016. Web.")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://breadboard.yale.edu/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/human-nature-lab/breadboard", comment=""))

    tool = Software(name="Empirica(beta)",
                    short_description="'Open source project to tackle the problem of long development cycles required to produce software to conduct multi-participant and real-time human experiments online.' link:https://github.com/empiricaly/meteor-empirica-core[Retrieved: 07.03.2019]",
                    developer="Nicolas Paton, & Abdullah Almaatouq",
                    maintainer="Nicolas Paton, & Abdullah Almaatouq",
                    softwarecategory=cat_oe,
                    architecture="server application",
                    license=lic_mit,
                    programminglanguages=[prol_js],
                    price="0",
                    recommandedcitation="Nicolas Paton, & Abdullah Almaatouq. (2018, November 15). Empirica: Open-Source, Real-Time, Synchronous, Virtual Lab Framework (Version v0.0.5). Zenodo. http://doi.org/10.5281/zenodo.1488413")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://empirica.ly/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/empiricaly/meteor-empirica-core", comment=""))

    tool = Software(name="SearchGazer",
                    short_description="SearchGazer: Webcam Eye Tracking for Remote Studies of Web Search",
                    developer="Alexandra Papoutsaki and James Laskey and Jeff Huang",
                    maintainer="Alexandra Papoutsaki and James Laskey and Jeff Huang",
                    softwarecategory=cat_eye,
                    architecture="framework",
                    license=lic_mit,
                    programminglanguages=[prol_js],
                    price="0",
                    recommandedcitation="@inproceedings{papoutsaki2017searchgazer, author = {Alexandra Papoutsaki and James Laskey and Jeff Huang}, title = {SearchGazer: Webcam Eye Tracking for Remote Studies of Web Search}, booktitle = {Proceedings of the ACM SIGIR Conference on Human Information Interaction \& Retrieval (CHIIR)}, year = {2017}, organization={ACM}} ")
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://nodegame.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/nodeGame", comment=""))

    tool = Software(name="NetLogo",
                    short_description="'NetLogo is a multi-agent programmable modeling environment. It is used by many tens of thousands of students, teachers and researchers worldwide. It also powers HubNet participatory simulations.'",
                    developer="Uri Wilensky",
                    maintainer="Uri Wilensky",
                    softwarecategory=cat_agent,
                    architecture="stand-alone application",
                    license=lic_unknown,
                    programminglanguages=[prol_java, prol_scala])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://ccl.northwestern.edu/netlogo/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/NetLogo/NetLogo", comment=""))

    tool = Software(name="DocumentCloud",
                    short_description="'DocumentCloud is a platform founded on the belief that if journalists were more open about their sourcing, the public would be more inclined to trust their reporting. The platform is a tool to help journalists share, analyze, annotate and, ultimately, publish source documents to the open web.' link:https://www.documentcloud.org/about[Source, visited: 04.03.2019]",
                    developer="Ted Han and Aron Pilhofer",
                    maintainer="Ted Han and Aron Pilhofer",
                    softwarecategory=cat_jour,
                    architecture="server application",
                    license=lic_mit,
                    programminglanguages=[prol_ruby])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.documentcloud.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/documentcloud/documentcloud", comment=""))

    tool = Software(name="NEW/S/LEAK",
                    short_description="'DocumentCloud is a platform founded on the belief that if journalists were more open about their sourcing, the public would be more inclined to trust their reporting. The platform is a tool to help journalists share, analyze, annotate and, ultimately, publish source documents to the open web.' link:https://www.documentcloud.org/about[Source, visited: 04.03.2019]",
                    developer="Gregor Wiedemann and Seid Muhie Yimam and Chris Biemann",
                    maintainer="Gregor Wiedemann and Seid Muhie Yimam and Chris Biemann",
                    softwarecategory=cat_jour,
                    architecture="server application",
                    license=lic_agpl3,
                    programminglanguages=[prol_ruby])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://www.documentcloud.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/documentcloud/documentcloud", comment=""))

    tool = Software(name="spades",
                    short_description="",
                    developer="",
                    maintainer="",
                    softwarecategory=cat_misc,
                    architecture="library",
                #    license=,
                    programminglanguages=[prol_r])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="http://spades.predictiveecology.org/", comment=""))
    db.session.add(Link(software=tool, type="repository", url="", comment=""))

    tool = Software(name="scikit-learn",
                    short_description="'Scikit-learn is a free software machine learning library for the Python programming language. It features various classification, regression and clustering algorithms including support vector machines, random forests, gradient boosting, k-means and DBSCAN, and is designed to interoperate with the Python numerical and scientific libraries NumPy and SciPy.'",
                    developer="Pedregosa, F. and Varoquaux, G. and Gramfort, A. and Michel, V. and Thirion, B. and Grisel, O. and Blondel, M. and Prettenhofer, P. and Weiss, R. and Dubourg, V. and Vanderplas, J. and Passos, A. and Cournapeau, D. and Brucher, M. and Perrot, M. and Duchesnay, E.",
                    maintainer="Pedregosa, F. and Varoquaux, G. and Gramfort, A. and Michel, V. and Thirion, B. and Grisel, O. and Blondel, M. and Prettenhofer, P. and Weiss, R. and Dubourg, V. and Vanderplas, J. and Passos, A. and Cournapeau, D. and Brucher, M. and Perrot, M. and Duchesnay, E.",
                    softwarecategory=cat_misc,
                    architecture="library",
                    license=lic_bsd,
                    programminglanguages=[prol_py])
    db.session.add(tool)
    db.session.add(Link(software=tool, type="website", url="https://scikit-learn.org/stable/index.html", comment=""))
    db.session.add(Link(software=tool, type="repository", url="https://github.com/scikit-learn/scikit-learn", comment=""))

    '''
     tool = Software(name="",
                     short_description="",
                     developer="",
                     maintainer="",
                     softwarecategory=cat_,
                     architecture="package",
                     license=lic_,
                     programminglanguages=[prol_])
     db.session.add(tool)
     db.session.add(Link(software=tool, type="website", url="", comment=""))
     db.session.add(Link(software=tool, type="repository", url="", comment=""))
     '''

    db.session.commit()
    return


if __name__ == '__main__':
    # Build a sample db on the fly, if one does not exist yet.
    app_dir = op.realpath(os.path.dirname(__file__))
    database_path = op.join(app_dir, app.config['DATABASE_FILE'])
    #if not os.path.exists(database_path):
    build_sample_db()
    # Start app
    app.run(debug=True)
